#ifndef HOW_TO_PLAY_H
#define HOW_TO_PLAY_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "functions_library/keys/keys.h"
#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace howtoplay
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_how_to_play
    {
        const int text1PosX = 280;
        const int text1PosY = 5;
        const int text1Size = 20;

        const int text2PosX = (screenWidth / 2) - 360;
        const int text2PosY = 80;
        const int text2Size = 100;

        const int rec1PosX = 15;
        const int rec2PosX = screenWidth - 130;
        const int recPosY = 120;
        const int recWidth = 110;
        const int recHeight = 10;

        const int arrowPosX1 = 80;
        const int arrowPosX2 = screenWidth - 150;
        const int arrowPosY = (screenHeight / 2) + 90;
        const int arrowSize = 40;

        const int text3PosX = (screenWidth / 2) - 300;
        const int text3PosY = screenHeight - 30;
        const int text3Size = 20;

        const int player1PosX = 190;
        const int player2PosX = 590;
        const int playerPosY = (screenHeight / 2) - 40;
        const int playerSize = 50;

        const int recPosX1 = 260;
        const int recPosY1 = (screenHeight / 2) + 50;
        const int recPosX2 = 660;
        const int recPosY2 = (screenHeight / 2) + 120;
        const int recSize = 60;
        const int recLineThick = 5;
    }
}

#endif