#include "how_to_play.h"

using namespace pong;
using namespace positions_how_to_play;

void pong::howtoplay::init()
{
}

void pong::howtoplay::update()
{
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::MENU;
    
    if (CheckCollisionPointRec(GetMousePosition(), { recPosX1, recPosY1, recSize, recSize }))
    {
        keys(pong::gameplay::key.upPlayer1, pong::gameplay::key.numberKeyUpPlayer1, pong::gameplay::key.numberKeyUpPlayer2, pong::gameplay::key.numberKeyDownPlayer1, pong::gameplay::key.numberKeyDownPlayer2);
    }
    else if (CheckCollisionPointRec(GetMousePosition(), { recPosX1, recPosY2, recSize, recSize }))
    {
        keys(pong::gameplay::key.downPlayer1, pong::gameplay::key.numberKeyDownPlayer1, pong::gameplay::key.numberKeyDownPlayer2, pong::gameplay::key.numberKeyUpPlayer1, pong::gameplay::key.numberKeyUpPlayer2);
    }
    else if (CheckCollisionPointRec(GetMousePosition(), { recPosX2, recPosY1, recSize, recSize }))
    {
        keys(pong::gameplay::key.upPlayer2, pong::gameplay::key.numberKeyUpPlayer2, pong::gameplay::key.numberKeyUpPlayer1, pong::gameplay::key.numberKeyDownPlayer2, pong::gameplay::key.numberKeyDownPlayer1);
    }
    else if (CheckCollisionPointRec(GetMousePosition(), { recPosX2, recPosY2, recSize, recSize }))
    {
        keys(pong::gameplay::key.downPlayer2, pong::gameplay::key.numberKeyDownPlayer2, pong::gameplay::key.numberKeyDownPlayer1, pong::gameplay::key.numberKeyUpPlayer2, pong::gameplay::key.numberKeyUpPlayer1);
    }

    showKeys(pong::gameplay::key.upPlayer1, 280, 70);
    showKeys(pong::gameplay::key.downPlayer1, 280, 140);
    showKeys(pong::gameplay::key.upPlayer2, 680, 70);
    showKeys(pong::gameplay::key.downPlayer2, 680, 140);
}

void pong::howtoplay::draw()
{
    DrawText("Press BACKSPACE to go to the main menu", text1PosX, text1PosY, text1Size, DARKGRAY);
    DrawText("HOW TO PLAY", text2PosX, text2PosY, text2Size, BLACK);
    DrawRectangle(rec1PosX, recPosY, recWidth, recHeight, RED);
    DrawRectangle(rec2PosX, recPosY, recWidth, recHeight, RED);
    DrawText("<--", arrowPosX1, arrowPosY, arrowSize, BLACK);
    DrawText("-->", arrowPosX2, arrowPosY, arrowSize, BLACK);
    DrawText("PUT THE MOUSE OVER THE KEY YOU WANT TO CHANGE", text3PosX, text3PosY, text3Size, DARKGRAY);

    DrawText("Player 1:", player1PosX, playerPosY, playerSize, BLACK);
    DrawRectangleLinesEx(Rectangle{ recPosX1, recPosY1, recSize, recSize }, recLineThick, BLACK);
    DrawRectangleLinesEx(Rectangle{ recPosX1, recPosY2, recSize, recSize }, recLineThick, BLACK);

    DrawText("Player 2:", player2PosX, playerPosY, playerSize, BLACK);
    DrawRectangleLinesEx(Rectangle{ recPosX2, recPosY1, recSize, recSize }, recLineThick, BLACK);
    DrawRectangleLinesEx(Rectangle{ recPosX2, recPosY2, recSize, recSize }, recLineThick, BLACK);
}

void pong::howtoplay::deinit()
{
}