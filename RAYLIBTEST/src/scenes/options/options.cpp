#include "options.h"

using namespace pong;
using namespace positions_options;

void pong::options::init()
{
}

void pong::options::update()
{
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::MENU;
    else if (IsKeyPressed(KEY_P))pong::game::currentScene = pong::game::SCENE::VICTORYPOINTS;
    else if (IsKeyPressed(KEY_C)) pong::game::currentScene = pong::game::SCENE::COLORS;
}

void pong::options::draw()
{
    DrawText("Press BACKSPACE to go to the main menu", text1PosX, text1PosY, text1Size, DARKGRAY);
    DrawText("OPTIONS", text2PosX, text2PosY, text2Size, BLACK);
    DrawRectangle(rec1PosX, recPosY, recWidth, recHeight, RED);
    DrawRectangle(rec2PosX, recPosY, recWidth, recHeight, RED);

    // Options
    DrawText("Press [P] to change victory points", text3PosX, text3PosY, textSize, BLACK);
    DrawText("Press [C] to change the color of", text4PosX, text4PosY, textSize, BLACK);
    DrawText("the ball and the paddles", text5PosX, text5PosY, textSize, BLACK);
}

void pong::options::deinit()
{
}