#ifndef OPTIONS_H
#define OPTIONS_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "functions_library/points/points.h"
#include "functions_library/colors/colors.h"
#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace options
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_options
    {
        const int text1PosX = 290;
        const int text1PosY = 5;
        const int text1Size = 20;

        const int text2PosX = (screenWidth / 2) - 230;
        const int text2PosY = 80;
        const int text2Size = 100;

        const int rec1PosX = 15;
        const int rec2PosX = screenWidth - 250;
        const int recPosY = 120;
        const int recWidth = 240;
        const int recHeight = 10;

        const int text3PosX = (screenWidth / 2) - 250;
        const int text3PosY = screenHeight / 2 - 20;
        const int text4PosX = (screenWidth / 2) - 240;
        const int text4PosY = screenHeight / 2 + 70;
        const int text5PosX = (screenWidth / 2) - 170;
        const int text5PosY = screenHeight / 2 + 120;
        const int textSize = 30;
    }
}

#endif