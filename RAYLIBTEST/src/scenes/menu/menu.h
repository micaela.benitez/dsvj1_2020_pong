#ifndef MENU_H
#define MENU_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "scenes/game/game.h"

namespace pong
{
	namespace menu
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_menu
    {
        const int text1PosX = (screenWidth / 2) - 210;
        const int text1PosY = 50;
        const int text1Size = 150;

        const int rec1PosX = 15;
        const int rec2PosX = screenWidth - 270;
        const int recPosY = 120;
        const int recWidth = 250;
        const int recHeight = 10;

        const int enterTextPosX = (screenWidth / 2) - 180;
        const int enterTextPosY = screenHeight / 2 - 40;
        const int ctrlTextPosX = (screenWidth / 2) - 200;
        const int ctrlTextPosY = screenHeight / 2 + 10;
        const int shiftTextPosX = (screenWidth / 2) - 250;
        const int shiftTextPosY = screenHeight / 2 + 60;
        const int altTextPosX = (screenWidth / 2) - 240;
        const int altTextPosY = screenHeight / 2 + 110;
        const int cTextPosX = (screenWidth / 2) - 220;
        const int cTextPosY = (screenHeight / 2) + 160;
        const int escTextPosX = (screenWidth / 2) - 150;
        const int escTextPosY = screenHeight / 2 + 210;
        const int optionsTextsSize = 30;

        const int versionPosX = screenWidth / 50;
        const int versionPosY = screenHeight - 40;
    }
}

#endif