#include "menu.h"

using namespace pong;
using namespace positions_menu;

void pong::menu::init()
{
}

void pong::menu::update()
{
    if (IsKeyPressed(KEY_ENTER)) pong::game::currentScene = pong::game::SCENE::PLAYERSMODE;
    else if (IsKeyPressed(KEY_LEFT_CONTROL) || IsKeyPressed(KEY_RIGHT_CONTROL)) pong::game::currentScene = pong::game::SCENE::OPTIONS;
    else if (IsKeyPressed(KEY_LEFT_SHIFT) || IsKeyPressed(KEY_RIGHT_SHIFT)) pong::game::currentScene = pong::game::SCENE::INSTRUCTIONS;
    else if (IsKeyPressed(KEY_LEFT_ALT) || IsKeyPressed(KEY_RIGHT_ALT)) pong::game::currentScene = pong::game::SCENE::HOWTOPLAY;
    else if (IsKeyPressed(KEY_C)) pong::game::currentScene = pong::game::SCENE::CREDITS;
}

void pong::menu::draw()
{
    DrawText("PONG", text1PosX, text1PosY, text1Size, BLACK);
    DrawRectangle(rec1PosX, recPosY, recWidth, recHeight, RED);
    DrawRectangle(rec2PosX, recPosY, recWidth, recHeight, RED);

    // Opciones
    DrawText("Press ENTER to PLAY", enterTextPosX, enterTextPosY, optionsTextsSize, BLACK);
    DrawText("Press CTRL to OPTIONS", ctrlTextPosX, ctrlTextPosY, optionsTextsSize, BLACK);
    DrawText("Press SHIFT to INSTRUCTIONS", shiftTextPosX, shiftTextPosY, optionsTextsSize, BLACK);
    DrawText("Press ALT to know how to play", altTextPosX, altTextPosY, optionsTextsSize, BLACK);
    DrawText("Press [C] to see the credits", cTextPosX, cTextPosY, optionsTextsSize, BLACK);
    DrawText("Press ESC to EXIT", escTextPosX, escTextPosY, optionsTextsSize, BLACK);

    DrawText("V 2.0", versionPosX, versionPosY, optionsTextsSize, GRAY);
}

void pong::menu::deinit()
{
}