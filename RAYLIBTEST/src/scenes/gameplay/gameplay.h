#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

#include "functions_library/entities/ball/ball.h"
#include "functions_library/points/points.h"
#include "functions_library/colors/colors.h"
#include "functions_library/keys/keys.h"
#include "functions_library/entities/paddles/paddles.h"
#include "functions_library/powerups/player_powerups.h"
#include "functions_library/powerups/random_powerups.h"
#include "functions_library/drawings/drawings.h"
#include "scenes/game/game.h"

namespace pong
{
	namespace gameplay
	{
        extern BALL ball;
        extern BALL ball2;
        extern PADDLES paddle;
        extern POINTS points;
        extern COLOR color;
        extern KEYS key;
        extern PLAYERPOWERUPS playerPowerup;
        extern RANDOMPOWERUPS randomPowerup;
        extern POWERUPACTIVE powerupActive;

        extern int rounds;
        extern int numberOfPlayers;

        extern Rectangle paddle1;
        extern Rectangle paddle2;

		void init();
        void update();
		void draw();
		void deinit();
	}

    namespace positions_gameplay
    {
        const int player1TextPosX = 5;
        const int player2TextPosX = screenWidth - 280;

        const int shield1RecPosX = 0;
        const int shield2RecPosX = screenWidth - 5;
    }
}

#endif