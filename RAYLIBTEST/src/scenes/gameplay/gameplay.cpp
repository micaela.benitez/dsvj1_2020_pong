#include "gameplay.h"

using namespace pong;
using namespace positions_gameplay;

namespace pong
{
    namespace gameplay
    {
        BALL ball;
        BALL ball2;
        PADDLES paddle;
        POINTS points;
        COLOR color;
        KEYS key;
        PLAYERPOWERUPS playerPowerup;
        RANDOMPOWERUPS randomPowerup;
        POWERUPACTIVE powerupActive;

        int rounds = 0;
        int numberOfPlayers = 0;

        Rectangle paddle1 = { paddle.posX1, paddle.posY1 , paddle.width, paddle.height };
        Rectangle paddle2 = { paddle.posX2, paddle.posY2 , paddle.width, paddle.height };
    }
}

void pong::gameplay::init ()
{
}

void pong::gameplay::update ()
{    
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::MENU;

    if (points.player1 < points.toWin && points.player2 < points.toWin)   // Si todavia nadie perdio
    {
        if (IsKeyPressed(KEY_SPACE)) ball.active = (ball.active == false) ? true : false;   // Pausa
        if (ball.active)
        {
            // Actualizacion del estado del circulo
            ballMotion(ball, points, paddle1, paddle2, playerPowerup);
            if (ball.loss) ballLoss(ball, paddle1, paddle2, numberOfPlayers, points);
            collisionCircleRec(ball, paddle1, paddle2, playerPowerup, powerupActive.reverseSpeed);

            // Actualizacion del estado de los rectangulos
            if (IsKeyDown(key.upPlayer1)) paddle1.y = rectangleMotionUp(paddle1);
            if (IsKeyDown(key.downPlayer1)) paddle1.y = rectangleMotionDown(paddle1, screenHeight);
            if (numberOfPlayers == 1) rectangleMotionPC(paddle2, ball, screenHeight);   // Contra la PC
            else   // De a dos
            {
                if (IsKeyDown(key.upPlayer2)) paddle2.y = rectangleMotionUp(paddle2);
                if (IsKeyDown(key.downPlayer2)) paddle2.y = rectangleMotionDown(paddle2, screenHeight);
            }

            // Potenciadores
            if (playerPowerup.paddlePlayer1) powerupPaddle(paddle1.height, paddle2.height, player1TextPosX);
            if (playerPowerup.paddlePlayer2) powerupPaddle(paddle2.height, paddle1.height, player2TextPosX);
            if (playerPowerup.speedPlayer1) powerupSpeed(player1TextPosX);
            if (playerPowerup.speedPlayer2) powerupSpeed(player2TextPosX);
            if (playerPowerup.shieldPlayer1) powerupShield(player1TextPosX, shield1RecPosX, color.paddle1);
            if (playerPowerup.shieldPlayer2) powerupShield(player2TextPosX, shield2RecPosX, color.paddle2);

            randomPowerups(randomPowerup, ball, ball2, key, color, points, paddle1, paddle2, playerPowerup, powerupActive);

            // Puntuacion
            if (points.player1 == points.toWin) points.gamesWonPlayer1++;
            else if (points.player2 == points.toWin) points.gamesWonPlayer2++;
            
            // Rondas
            if (points.player1 == points.toWin || points.player2 == points.toWin) rounds++;
        }
    }
}

void pong::gameplay::draw ()
{
    if (points.player1 < points.toWin && points.player2 < points.toWin)
    {       
        drawScreenDetails(points, ball, randomPowerup);
        drawFigures(paddle1, paddle2, ball, color);
        drawRandomPowerups(randomPowerup, ball, ball2, powerupActive);
    }
    else
    {
        pong::game::currentScene = pong::game::SCENE::WINNER;
    }
}

void pong::gameplay::deinit ()
{
    // Inicializacion
    const int initializeBallPosX = (float)screenWidth / 2;
    const int initializeBallPosY = (float)screenHeight / 2;

    const int initializeTimer = 0;
    const int initializePointsPlayers = 0;

    const int initializePaddleHeight = 120;
    const int initializePaddle1PosX = 5;
    const int initializePaddle2PosX = screenWidth - 20;
    const int initializePaddlePosY = (float)((screenHeight / 2) - (paddle1.height / 2));  

    const int obstaclePosX = (screenWidth / 2) - 10;
    const int obstaclePosY = 0;
    
    ball.position.x = initializeBallPosX;
    ball.position.y = initializeBallPosY;
    ball.loss = true;
    ball.active = true;

    points.player1 = initializePointsPlayers;
    points.player2 = initializePointsPlayers;
    
    paddle1.height = initializePaddleHeight;
    paddle1.x = initializePaddle1PosX;
    paddle1.y = initializePaddlePosY;

    paddle2.height = initializePaddleHeight;
    paddle2.x = initializePaddle2PosX;
    paddle2.y = initializePaddlePosY;

    playerPowerup.paddlePlayer1 = false;
    playerPowerup.speedPlayer1 = false;
    playerPowerup.shieldPlayer1 = false;
    playerPowerup.paddlePlayer2 = false;
    playerPowerup.speedPlayer2 = false;
    playerPowerup.shieldPlayer2 = false;

    randomPowerup.timer = initializeTimer;
    powerupActive.multiball = false;
    powerupActive.reverseSpeed = false;
    keys(key.upPlayer1, key.numberKeyUpPlayer1);
    keys(key.upPlayer2, key.numberKeyUpPlayer2);
    keys(key.downPlayer1, key.numberKeyDownPlayer1);
    keys(key.downPlayer2, key.numberKeyDownPlayer2);

    pong::obstacle::posX = obstaclePosX;
    pong::obstacle::posY = obstaclePosY;
    pong::obstacle::up = false;
    pong::obstacle::down = true;
}