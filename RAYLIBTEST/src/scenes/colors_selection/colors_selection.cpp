#include "colors_selection.h"

using namespace pong;
using namespace positions_color_selection;

void pong::colorselection::init()
{
}

void pong::colorselection::update()
{
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::OPTIONS;
    
    if (CheckCollisionPointRec(GetMousePosition(), { recPosX1, recPosY, recWidth, recHeight }))
    {
        colors(pong::gameplay::color.numberColorPaddle1, pong::gameplay::color.paddle1);
    }

    if (CheckCollisionPointCircle(GetMousePosition(), { ballPosX, ballPosY }, ballRadius))
    {
        colors(pong::gameplay::color.numberColorBall, pong::gameplay::color.ball);
    }

    if (CheckCollisionPointRec(GetMousePosition(), { recPosX2, recPosY, recWidth, recHeight }))
    {
        colors(pong::gameplay::color.numberColorPaddle2, pong::gameplay::color.paddle2);
    }
}

void pong::colorselection::draw()
{
    DrawText("Press BACKSPACE to go to options", text1PosX, text1PosY, text1Size, DARKGRAY);
    DrawText("Select a color for the ball", text2PosX, text2PosY, text2Size, BLACK);
    DrawText("and for the paddles", text3PosX, text3PosY, text3Size, BLACK);
    DrawRectangle(rec1PosX, rec1PosY, rec1Width, rec1Height, RED);
    DrawRectangle(rec2PosX, rec1PosY, rec1Width, rec1Height, RED);
    DrawText("<--", arrox1PosX, arrowPosY, arrowSize, BLACK);
    DrawText("-->", arrow2PosX, arrowPosY, arrowSize, BLACK);
    DrawText("PUT THE MOUSE OVER THE FIGURE YOU WANT TO CHANGE COLOR", text4PosX, text4PosY, text4Size, DARKGRAY);

    DrawRectangle(recPosX1, recPosY, recWidth, recHeight, pong::gameplay::color.paddle1);
    DrawRectangle(recPosX2, recPosY, recWidth, recHeight, pong::gameplay::color.paddle2);
    DrawCircle(ballPosX, ballPosY, ballRadius, pong::gameplay::color.ball);
}

void pong::colorselection::deinit()
{
}