#ifndef COLORS_SELECTION_H
#define COLORS_SELECTION_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "functions_library/colors/colors.h"
#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace colorselection
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_color_selection
    {
        const int text1PosX = 320;
        const int text1PosY = 5;
        const int text1Size = 20;

        const int text2PosX = (screenWidth / 2) - 340;
        const int text2PosY = 100;
        const int text2Size = 50;

        const int text3PosX = (screenWidth / 2) - 250;
        const int text3PosY = 150;
        const int text3Size = 50;

        const int rec1PosX = 15;
        const int rec2PosX = screenWidth - 140;
        const int rec1PosY = 120;
        const int rec1Width = 120;
        const int rec1Height = 10;

        const int arrox1PosX = 100;
        const int arrow2PosX = screenWidth - 150;
        const int arrowPosY = (screenHeight / 2) + 40;
        const int arrowSize = 40;

        const int text4PosX = (screenWidth / 2) - 350;
        const int text4PosY = screenHeight - 30;
        const int text4Size = 20;

        const int recPosX1 = (screenWidth / 2) - 210;
        const int recPosX2 = (screenWidth / 2) + 200;
        const int recPosY = (screenHeight / 2) - 25;
        const int recWidth = 20;
        const int recHeight = 150;

        const int ballPosX = screenWidth / 2;
        const int ballPosY = (screenHeight / 2) + 50;
        const int ballRadius = 25;
    }
}

#endif 