#include "winner.h"

using namespace pong;
using namespace positions_winner;

void pong::winner::init()
{
}

void pong::winner::update()
{
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::MENU;
    else if (IsKeyPressed(KEY_ENTER))
    {
        pong::game::currentScene = pong::game::SCENE::GAMEPLAY;
        pong::gameplay::deinit();
    }
}

void pong::winner::draw()
{
    if (pong::gameplay::points.player1 == pong::gameplay::points.toWin) DrawText("PLAYER 1 WINS!!!", winnerPosX, winnerPosY, winnerSize, LIME);
    else DrawText("PLAYER 2 WINS!!!", winnerPosX, winnerPosY, winnerSize, LIME);
    
    DrawText("Press BACKSPACE to go to the main menu", text1PosX, text1PosY, textSize, DARKGRAY);
    DrawText("Press ENTER to play again", text2PosX, text2PosY, textSize, DARKGRAY);
    DrawText(TextFormat("Total games won player 1: %i", pong::gameplay::points.gamesWonPlayer1), gamesWon1PosX, gamesWon1PosY, informationTextsSize, DARKGRAY);
    DrawText(TextFormat("Total games won player 2: %i", pong::gameplay::points.gamesWonPlayer2), gamesWon2PosX, gamesWon2PosY, informationTextsSize, DARKGRAY);
    DrawText(TextFormat("Total rounds: %i", pong::gameplay::rounds), roundsPosX, roundsPosY, informationTextsSize, DARKGRAY);
}

void pong::winner::deinit()
{
}