#ifndef WINNER_H
#define WINNER_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace winner
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_winner
    {
        const int winnerPosX = 80;
        const int winnerPosY = 200;
        const int winnerSize = 100;

        const int gamesWon1PosX = (screenHeight / 2) - 10;
        const int gamesWon1PosY = 440;
        const int gamesWon2PosX = (screenHeight / 2) - 15;
        const int gamesWon2PosY = 490;
        const int roundsPosX = (screenHeight / 2) + 70;
        const int roundsPosY = 540;
        const int informationTextsSize = 30;

        const int text1PosX = 280;
        const int text1PosY = 5;
        const int text2PosX = 350;
        const int text2PosY = 40;
        const int textSize = 20;
    }
}

#endif