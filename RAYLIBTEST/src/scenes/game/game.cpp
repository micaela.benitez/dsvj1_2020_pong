#include "game.h"

namespace pong
{
	namespace game
	{
		SCENE currentScene;
	}
}

void pong::game::init()
{
	InitWindow(screenWidth, screenHeight, "PONG");
	SetTargetFPS(60);   // 60 cuadros por segundo

	menu::init();
	gameplay::init();
	playersmode::init();
	options::init();
	victorypoints::init();
	colorselection::init();
	instructions::init();
	instructions2::init();
	howtoplay::init();
	credits::init();
	winner::init();
}

void pong::game::update()
{
	switch (currentScene)
	{
	case SCENE::MENU:
		menu::update();
		break;
	case SCENE::GAMEPLAY:
		gameplay::update();
		break;
	case SCENE::PLAYERSMODE:
		playersmode::update();
		break;
	case SCENE::OPTIONS:
		options::update();
		break;
	case SCENE::VICTORYPOINTS:
		victorypoints::update();
		break;
	case SCENE::COLORS:
		colorselection::update();
		break;
	case SCENE::INSTRUCTIONS:
		instructions::update();
		break;
	case SCENE::INSTRUCTIONS2:
		instructions2::update();
		break;
	case SCENE::HOWTOPLAY:
		howtoplay::update();
		break;
	case SCENE::CREDITS:
		credits::update();
		break;
	case SCENE::WINNER:
		winner::update();
		break;
	}
}

void pong::game::draw()
{
	BeginDrawing();
	ClearBackground(BEIGE);

	switch (currentScene)
	{
	case SCENE::MENU:
		menu::draw();
		break;
	case SCENE::GAMEPLAY:
		gameplay::draw();
		break;
	case SCENE::PLAYERSMODE:
		playersmode::draw();
		break;
	case SCENE::OPTIONS:
		options::draw();
		break;
	case SCENE::VICTORYPOINTS:
		victorypoints::draw();
		break;
	case SCENE::COLORS:
		colorselection::draw();
		break;
	case SCENE::INSTRUCTIONS:
		instructions::draw();
		break;
	case SCENE::INSTRUCTIONS2:
		instructions2::draw();
		break;
	case SCENE::HOWTOPLAY:
		howtoplay::draw();
		break;
	case SCENE::CREDITS:
		credits::draw();
		break;
	case SCENE::WINNER:
		winner::draw();
		break;
	}

	EndDrawing();
}

void pong::game::deinit()
{
	CloseWindow();

	menu::deinit();
	gameplay::deinit();
	playersmode::deinit();
	options::deinit();
	victorypoints::deinit();
	colorselection::deinit();
	instructions::deinit();
	instructions2::deinit();
	howtoplay::deinit();
	credits::deinit();
	winner::deinit();
}

void pong::game::run()
{
	init();

	while (!WindowShouldClose())
	{
		update();
		draw();
	}

	deinit();
}