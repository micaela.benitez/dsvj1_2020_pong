#ifndef GAME_H
#define GAME_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/menu/menu.h"
#include "scenes/players_mode/choose_players_mode.h"
#include "scenes/options/options.h"
#include "scenes/victory_points/victory_points.h"
#include "scenes/colors_selection/colors_selection.h"
#include "scenes/instructions/instructions.h"
#include "scenes/how_to_play/how_to_play.h"
#include "scenes/credits/credits.h"
#include "scenes/winner/winner.h"

namespace pong
{
	namespace game
	{
		enum class SCENE { MENU, PLAYERSMODE, GAMEPLAY, OPTIONS, VICTORYPOINTS, COLORS, INSTRUCTIONS, INSTRUCTIONS2, HOWTOPLAY, CREDITS, WINNER};

		extern SCENE currentScene;		

		void init();
		void update();
		void draw();
		void deinit();

		void run();
	}
}

#endif