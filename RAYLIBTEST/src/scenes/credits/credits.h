#ifndef CREDITS_H
#define CREDITS_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace credits
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_credits
    {
        const int text1PosX = 280;
        const int text1PosY = 5;
        const int text1Size = 20;

        const int text2PosX = (screenWidth / 2) - 210;
        const int text2PosY = 70;
        const int text2Size = 110;

        const int rec1PosX = 15;
        const int rec2PosX = screenWidth - 280;
        const int recPosY = 120;
        const int recWidth = 260;
        const int recHeight = 10;

        const int creditsNamePosX = 340;
        const int creditsNamePosY1 = 290;
        const int creditsNamePosY2 = 390;
        const int creditsNamePosY3 = 490;
        const int creditsPosX1 = 400;
        const int creditsPosX2 = 440;
        const int creditsPosX3 = 380;
        const int creditsPosY1 = 260;
        const int creditsPosY2 = 360;
        const int creditsPosY3 = 460;
        const int creditsSize = 30;
    }
}

#endif 