#include "credits.h"

using namespace pong;
using namespace positions_credits;

void pong::credits::init()
{
}

void pong::credits::update()
{
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::MENU;
}

void pong::credits::draw()
{
    DrawText("Press BACKSPACE to go to the main menu", text1PosX, text1PosY, text1Size, DARKGRAY);
    DrawText("Credits", text2PosX, text2PosY, text2Size, BLACK);
    DrawRectangle(rec1PosX, recPosY, recWidth, recHeight, RED);
    DrawRectangle(rec2PosX, recPosY, recWidth, recHeight, RED);

    // Creditos
    DrawText("Programmer", creditsPosX1, creditsPosY1, creditsSize, BLACK);
    DrawText("Micaela Luz Benitez", creditsNamePosX, creditsNamePosY1, creditsSize, BLACK);
    DrawText("Artist", creditsPosX2, creditsPosY2, creditsSize, BLACK);
    DrawText("Micaela Luz Benitez", creditsNamePosX, creditsNamePosY2, creditsSize, BLACK);
    DrawText("Game Designer", creditsPosX3, creditsPosY3, creditsSize, BLACK);
    DrawText("Micaela Luz Benitez", creditsNamePosX, creditsNamePosY3, creditsSize, BLACK);
}

void pong::credits::deinit()
{
}