#include "victory_points.h"

using namespace pong;
using  namespace positions_victory_points;

void pong::victorypoints::init()
{
}

void pong::victorypoints::update()
{
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::OPTIONS;

    if (IsKeyPressed(KEY_ONE)) pong::gameplay::points.toWin = 8;
    else if (IsKeyPressed(KEY_TWO)) pong::gameplay::points.toWin = 12;
    else if (IsKeyPressed(KEY_THREE)) pong::gameplay::points.toWin = 16;
    else if (IsKeyPressed(KEY_FOUR)) pong::gameplay::points.toWin = 20;
}

void pong::victorypoints::draw()
{
    DrawText("Press BACKSPACE to go to options", text1PosX, text1PosY, text1Size, DARKGRAY);
    DrawText("Change victory points", text2PosX, text2PosY, text2Size, BLACK);
    DrawRectangle(rec1PosX, recPosY, recWidth, recHeight, RED);
    DrawRectangle(rec2PosX, recPosY, recWidth, recHeight, RED);

    // Options
    DrawText("Press 1 to set 8 points", pointsTextPosX1, pointsTextPosY1, pointsTextSize, BLACK);
    DrawText("Press 2 to set 12 points", pointsTextPosX2, pointsTextPosY2, pointsTextSize, BLACK);
    DrawText("Press 3 to set 16 points", pointsTextPosX3, pointsTextPosY3, pointsTextSize, BLACK);
    DrawText("Press 4 to set 20 points", pointsTextPosX4, pointsTextPosY4, pointsTextSize, BLACK);
    DrawText(TextFormat("Current victory points: %i", pong::gameplay::points.toWin), currectPointsTextPosX, currectPointsTextPosY, currectPointsTextSize, BLACK);
}

void pong::victorypoints::deinit()
{
}