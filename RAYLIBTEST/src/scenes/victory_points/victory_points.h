#ifndef VICTORY_POINTS_H
#define VICTORY_POINTS_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "functions_library/points/points.h"
#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace victorypoints
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_victory_points
    {
        const int text1PosX = 320;
        const int text1PosY = 5;
        const int text1Size = 20;

        const int text2PosX = (screenWidth / 2) - 280;
        const int text2PosY = 100;
        const int text2Size = 50;

        const int rec1PosX = 15;
        const int rec2PosX = screenWidth - 210;
        const int recPosY = 120;
        const int recWidth = 190;
        const int recHeight = 10;

        const int pointsTextPosX1 = (screenWidth / 2) - 185;
        const int pointsTextPosY1 = 245;
        const int pointsTextPosX2 = (screenWidth / 2) - 195;
        const int pointsTextPosY2 = 285;
        const int pointsTextPosX3 = (screenWidth / 2) - 195;
        const int pointsTextPosY3 = 325;
        const int pointsTextPosX4 = (screenWidth / 2) - 200;
        const int pointsTextPosY4 = 365;
        const int pointsTextSize = 30;

        const int currectPointsTextPosX = (screenWidth / 2) - 270;
        const int currectPointsTextPosY = 470;
        const int currectPointsTextSize = 40;
    }
}

#endif