#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace instructions
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

	namespace instructions2
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_instructions
    {
        // Variables instrucciones 1
        const int text1PosX = 280;
        const int text1PosY = 5;

        const int text2PosX = 300;
        const int text2PosY = 35;

        const int text3PosX = (screenWidth / 2) - 400;
        const int text3PosY = 80;

        const int rec1PosX = 15;
        const int rec2PosX = screenWidth - 90;
        const int recPosY = 120;
        const int recWidth = 70;
        const int recHeight = 10;

        const int text4PosX = 50;
        const int text4PosY = 230;

        const int text5PosX = 350;
        const int text5PosY = 270;

        const int text6PosX = 10;
        const int text6PosY = 340;

        const int text7PosX = 25;
        const int text7PosY = 380;

        const int text8PosX = 20;
        const int text8PosY = 420;

        const int text9PosX = 35;
        const int text9PosY = 460;

        const int text10PosX = 200;
        const int text10PosY = 530;

        const int text11PosX = 250;
        const int text11PosY = 570;

        const int textSize = 20;
        const int textSize2 = 30;
        const int textSize3 = 100;

        // Variables instrucciones 2 que no coinciden con las variables de instrucciones 1
        const int text3PosXBis = (screenWidth / 2) - 280;

        const int rec2PosXBis = screenWidth - 205;
        const int recWidthBis = 185;

        const int text4PosXBis = 100;

        const int text5PosXBis = 140;
        const int text5PosYBis = 275;

        const int text6PosXBis = 30;
        const int text6PosYBis = 320;

        const int text7PosXBis = 20;
        const int text7PosYBis = 365;

        const int text8PosXBis = 260;
        const int text8PosYBis = 410;

        const int text9PosXBis = 300;
        const int text9PosYBis = 455;

        const int text10PosXBis = 250;
        const int text10PosYBis = 500;

        const int text11PosXBis = 130;
        const int text11PosYBis = 545;

        const int circleRedPosX = 230;
        const int circleRedPosX2 = 760;
        const int circleRedPosY = 425;

        const int circleBluePosX = 270;
        const int circleBluePosX2 = 730;
        const int circleBluePosY = 470;

        const int circleGreenPosX = 220;
        const int circleGreenPosX2 = 770;
        const int circleGreenPosY = 515;

        const int circleRadius = 10;
    }
}

#endif