#include "instructions.h"

using namespace pong;
using namespace positions_instructions;

void pong::instructions::init()
{
}

void pong::instructions::update()
{
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::MENU;
    else if (IsKeyPressed(KEY_ENTER)) pong::game::currentScene = pong::game::SCENE::INSTRUCTIONS2;
}

void pong::instructions::draw()
{
    DrawText("Press BACKSPACE to go to the main menu", text1PosX, text1PosY, textSize, DARKGRAY);
    DrawText("Press ENTER to see more instructions", text2PosX, text2PosY, textSize, DARKGRAY);
    DrawText("INSTRUCTIONS", text3PosX, text3PosY, textSize3, BLACK);
    DrawRectangle(rec1PosX, recPosY, recWidth, recHeight, RED);
    DrawRectangle(rec2PosX, recPosY, recWidth, recHeight, RED);

    // Instrucciones
    DrawText("Welcome to the famous two-dimensional game that simulates", text4PosX, text4PosY, textSize2, BLACK);
    DrawText("the table tennis!!!", text5PosX, text5PosY, textSize2, BLACK);
    DrawText("What is this game about? Players will control the black paddles", text6PosX, text6PosY, textSize2, BLACK);
    DrawText(" by them only vertically, hitting the ball from one lake to the", text7PosX, text7PosY, textSize2, BLACK);
    DrawText("other. If one of the two misses the ball, the opponent wins a", text8PosX, text8PosY, textSize2, BLACK);
    DrawText("point. The first to reach a certain number of points wins!", text9PosX, text9PosY, textSize2, BLACK);
    DrawText("(The number of points to win is predetermined at 12,", text10PosX, text10PosY, textSize, BLACK);
    DrawText("but in settings it can be changed if desired)", text11PosX, text11PosY, textSize, BLACK);
}

void pong::instructions::deinit()
{
}


void pong::instructions2::init()
{
}

void pong::instructions2::update()
{
    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::MENU;
    else if (IsKeyPressed(KEY_ENTER)) pong::game::currentScene = pong::game::SCENE::INSTRUCTIONS;
}

void pong::instructions2::draw()
{
    DrawText("Press BACKSPACE to go to the main menu", text1PosX, text1PosY, textSize, DARKGRAY);
    DrawText("Press ENTER to see more instructions", text2PosX, text2PosY, textSize, DARKGRAY);
    DrawText("POWERUPS", text3PosXBis, text3PosY, textSize3, BLACK);
    DrawRectangle(rec1PosX, recPosY, recWidthBis, recHeight, RED);
    DrawRectangle(rec2PosXBis, recPosY, recWidthBis, recHeight, RED);

    // Instrucciones de los potenciadores
    DrawText("Players powerups: are activated by adding points", text4PosXBis, text4PosY, textSize2, BLACK);
    DrawText("- Big paddle       - Faster ball       - Shield", text5PosXBis, text5PosYBis, textSize2, DARKGRAY);
    DrawText("Random powerups: every 15 minutes a different one appears,", text6PosXBis, text6PosYBis, textSize2, BLACK);
    DrawText("the circular ones are activated by touching them with the ball", text7PosXBis, text7PosYBis, textSize2, BLACK);
    DrawText("Multiball, another ball appears", text8PosXBis, text8PosYBis, textSize2, DARKGRAY);
    DrawText("Reverse speed of the ball", text9PosXBis, text9PosYBis, textSize2, DARKGRAY);
    DrawText("Reverse controls of the paddle", text10PosXBis, text10PosYBis, textSize2, DARKGRAY);
    DrawText("Rectangular obstacle in the field with movement", text11PosXBis, text11PosYBis, textSize2, DARKGRAY);

    // Circulos de cada potenciador random
    DrawCircleGradient(circleRedPosX, circleRedPosY, circleRadius, RED, BLACK);
    DrawCircleGradient(circleRedPosX2, circleRedPosY, circleRadius, RED, BLACK);
    DrawCircleGradient(circleBluePosX, circleBluePosY, circleRadius, BLUE, BLACK);
    DrawCircleGradient(circleBluePosX2, circleBluePosY, circleRadius, BLUE, BLACK);
    DrawCircleGradient(circleGreenPosX, circleGreenPosY, circleRadius, GREEN, BLACK);
    DrawCircleGradient(circleGreenPosX2, circleGreenPosY, circleRadius, GREEN, BLACK);
}

void pong::instructions2::deinit()
{
}
