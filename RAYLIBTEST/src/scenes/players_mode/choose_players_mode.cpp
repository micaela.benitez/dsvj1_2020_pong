#include "choose_players_mode.h"

using namespace pong;
using namespace positions_choose_players_mode;

void pong::playersmode::init()
{
}

void pong::playersmode::update()
{
    if (IsKeyPressed(KEY_ONE)) pong::gameplay::numberOfPlayers = 1;
    else if (IsKeyPressed(KEY_TWO)) pong::gameplay::numberOfPlayers = 2;

    if (IsKeyPressed(KEY_BACKSPACE)) pong::game::currentScene = pong::game::SCENE::MENU;
    else if (IsKeyPressed(KEY_ONE) || IsKeyPressed(KEY_TWO))
    {
        pong::game::currentScene = pong::game::SCENE::GAMEPLAY;
        pong::gameplay::deinit();
    }
}

void pong::playersmode::draw()
{
    DrawText("Press BACKSPACE to go to the main menu", text1PosX, text1PosY, text1Size, DARKGRAY);
    DrawText("PONG", text2PosX, text2PosY, text2Size, BLACK);
    DrawRectangle(rec1PosX, recPosY, recWidth, recHeight, RED);
    DrawRectangle(rec2PosX, recPosY, recWidth, recHeight, RED);

    // Options
    DrawText("Press 1 to play against the pc", playersTextPosX1, playersTextPosY1, playersTextSize, BLACK);
    DrawText("Press 2 to play in pairs", playersTextPosX2, playersTextPosY2, playersTextSize, BLACK);
}

void pong::playersmode::deinit()
{
}