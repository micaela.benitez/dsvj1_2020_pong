#ifndef CHOOSE_PLAYERS_MODE_H
#define CHOOSE_PLAYERS_MODE_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"
#include "scenes/game/game.h"
#include "scenes/gameplay/gameplay.h"

namespace pong
{
	namespace playersmode
	{
		void init();
		void update();
		void draw();
		void deinit();
	}

    namespace positions_choose_players_mode
    {
        const int text1PosX = 280;
        const int text1PosY = 5;
        const int text1Size = 20;

        const int text2PosX = (screenWidth / 2) - 210;
        const int text2PosY = 50;
        const int text2Size = 150;

        const int rec1PosX = 15;
        const int rec2PosX = screenWidth - 270;
        const int recPosY = 120;
        const int recWidth = 250;
        const int recHeight = 10;

        const int playersTextPosX1 = (screenWidth / 2) - 230;
        const int playersTextPosY1 = screenHeight / 2;
        const int playersTextPosX2 = (screenWidth / 2) - 190;
        const int playersTextPosY2 = (screenHeight / 2) + 100;
        const int playersTextSize = 30;
    }
}

#endif