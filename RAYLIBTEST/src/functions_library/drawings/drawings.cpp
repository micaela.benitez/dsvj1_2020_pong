#include "drawings.h"

using namespace pong;
using namespace positions_drawings;

void drawScreenDetails(POINTS points, BALL ball, RANDOMPOWERUPS randomPowerups)
{
    DrawRectangle(recPosX, recPosY, recWidth, recHeight, LIGHTGRAY);
    DrawText(TextFormat("P1: %i", points.player1), points1PosX, pointsPosY, pointsSize, LIGHTGRAY);
    DrawText(TextFormat("P2: %i", points.player2), points2PosX, pointsPosY, pointsSize, LIGHTGRAY);
    DrawText(TextFormat("Time: %i seg", randomPowerups.timer), textTimerPosX, textTimerPosY, textSize, DARKGRAY);
    DrawText("Press BACKSPACE to go", text1PosX, text1PosY, textSize, DARKGRAY);
    DrawText("back to the main menu", text1PosX, text1BisPosY, textSize, DARKGRAY);
    DrawText("Press SPACE to pause", text2PosX, text2PosY, textSize, DARKGRAY);
    DrawText("Press ESCAPE to exit", text3PosX, text3PosY, textSize, DARKGRAY);

    if (!ball.active)
    {
        DrawText("PAUSE", pausePosX, pausePosY, pauseSize, GRAY);
    }

    if (ball.loss)
    {
        if (points.player1 == 0 && points.player2 == 0)
        {
            DrawText("Press ENTER to start the game", enterPosX, enterPosY, enterSize, GRAY);
        }
        else
        {
            DrawText("Press ENTER to throw the ball", enterPosX, enterPosY, enterSize, GRAY);
        }
    }
}

void drawFigures(Rectangle paddle1, Rectangle paddle2, BALL ball, COLOR color)
{
    DrawRectangleRec(paddle1, color.paddle1);
    DrawRectangleRec(paddle2, color.paddle2);
    DrawCircleV(ball.position, ball.radius, color.ball);
}