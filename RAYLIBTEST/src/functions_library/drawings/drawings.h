#ifndef DRAWINGS_H
#define DRAWINGS_H

#include "raylib.h"

#include "functions_library/points/points.h"
#include "functions_library/entities/ball/ball.h"
#include "functions_library/colors/colors.h"
#include "functions_library/powerups/random_powerups.h"

namespace pong
{
    namespace positions_drawings
    {
        const int recPosX = (screenWidth / 2) - 5;
        const int recPosY = 0;
        const int recWidth = 10;
        const int recHeight = screenHeight;

        const int points1PosX = (screenWidth / 2) - 200;
        const int points2PosX = (screenWidth / 2) + 40;
        const int pointsPosY = 0;
        const int pointsSize = 80;

        const int textTimerPosX = screenWidth - 150;
        const int textTimerPosY = 5;
        
        const int text1PosX = 10;
        const int text1PosY = 5;
        const int text1BisPosY = 25;
        const int text2PosX = (screenWidth / 2) - 250;
        const int text2PosY = screenHeight - 20;
        const int text3PosX = (screenWidth / 2) + 15;
        const int text3PosY = screenHeight - 20;
        const int textSize = 20;

        const int pausePosX = (screenWidth / 2) - 170;
        const int pausePosY = (screenHeight / 2) - 50;
        const int pauseSize = 100;

        const int enterPosX = (screenWidth / 2) - 240;
        const int enterPosY = (screenHeight / 2) - 50;
        const int enterSize = 30;
    }
}

void drawScreenDetails(POINTS points, BALL ball, RANDOMPOWERUPS randomPowerups);
void drawFigures(Rectangle paddle1, Rectangle paddle2, BALL ball, COLOR color);

#endif