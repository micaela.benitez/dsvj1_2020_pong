#include "colors.h"

void colors(int& numberColor, Color& colorFigure)
{
    // Opcion colores
    if (IsKeyPressed(KEY_LEFT)) numberColor--;
    else if (IsKeyPressed(KEY_RIGHT)) numberColor++;

    if (numberColor == 1) colorFigure = BLACK;
    else if (numberColor == 2) colorFigure = YELLOW;
    else if (numberColor == 3) colorFigure = ORANGE;
    else if (numberColor == 4) colorFigure = PINK;
    else if (numberColor == 5) colorFigure = RED;
    else if (numberColor == 6) colorFigure = GREEN;
    else if (numberColor == 7) colorFigure = DARKGREEN;
    else if (numberColor == 8) colorFigure = SKYBLUE;
    else if (numberColor == 9) colorFigure = DARKBLUE;
    else if (numberColor == 10) colorFigure = PURPLE;
    else if (numberColor == 11) colorFigure = DARKPURPLE;
    else if (numberColor == 12) colorFigure = BROWN;
    else if (numberColor == 13) colorFigure = DARKBROWN;
    else if (numberColor == 14) colorFigure = WHITE;

    if (numberColor == 0) numberColor = 14;
    else if (numberColor == 15) numberColor = 1;
}