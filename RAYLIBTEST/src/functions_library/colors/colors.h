#ifndef COLORS_H
#define COLORS_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"

struct COLOR
{
	Color paddle1 = BLACK;
	Color paddle2 = BLACK;
	Color ball = RED;
	int numberColorPaddle1 = 1;
	int numberColorPaddle2 = 1;
	int numberColorBall = 5;
};

void colors(int& numberColor, Color& colorFigure);

#endif