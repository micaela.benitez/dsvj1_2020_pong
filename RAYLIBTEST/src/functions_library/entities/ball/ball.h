#ifndef BALL_H
#define BALL_H

#include "functions_library/screen_size/screen_size.h"

struct BALL
{
    Vector2 position = { (float)screenWidth / 2, (float)screenHeight / 2 };
    Vector2 speed = { (float)5.0f, (float)7.0f };
    bool active = true;
    bool loss = true;
    int radius = 15;
};

#endif