#ifndef PADDLES_H
#define PADDLES_H

#include "functions_library/screen_size/screen_size.h"

struct PADDLES
{
    float width = 15;
    float height = 120;
    float posX1 = 5;
    float posX2 = screenWidth - 20;
    float posY1 = (float)((screenHeight / 2) - (height / 2));
    float posY2 = (float)((screenHeight / 2) - (height / 2));
};

#endif