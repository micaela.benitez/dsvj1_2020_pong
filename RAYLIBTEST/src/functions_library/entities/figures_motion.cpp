#include "figures_motion.h"

using namespace pong;
using namespace speed_figures_motion;

int rectangleMotionUp(Rectangle& paddle)
{
    return paddle.y = ((paddle.y - paddlesSpeed) > 0.0f) ? paddle.y -= paddlesSpeed : paddle.y = 0.0f;
}

int rectangleMotionDown(Rectangle& paddle, int screenHeight)
{
    return paddle.y = ((paddle.y + paddlesSpeed) < (screenHeight - paddle.height)) ? paddle.y += paddlesSpeed : paddle.y = screenHeight - paddle.height;
}

int rectangleMotionPC(Rectangle& paddle, BALL ball, int screenHeight)
{
    if (ball.position.y < paddle.y) return paddle.y = ((paddle.y - paddlesSpeedIA) > 0.0f) ? paddle.y -= paddlesSpeedIA : paddle.y = 0.0f;
    else return paddle.y = ((paddle.y + paddlesSpeedIA) < (screenHeight - paddle.height)) ? paddle.y += paddlesSpeedIA : paddle.y = screenHeight - paddle.height;
}

void ballMotion(BALL& ball, POINTS& points, Rectangle& paddle1, Rectangle& paddle2, PLAYERPOWERUPS& playerPowerup)
{
    if (!ball.loss)
    {
        ball.position.x += ball.speed.x;
        ball.position.y += ball.speed.y;
    }

    if ((ball.position.y >= (GetScreenHeight() - ball.radius)) || (ball.position.y <= ball.radius)) ball.speed.y *= -1.0f;   // Si se va para arriba
    if ((ball.position.x >= (GetScreenWidth() - ball.radius)) || (ball.position.x <= ball.radius))   // Si se va para un costado
    {
        if (ball.speed.x > 0)
        {
            if (!playerPowerup.shieldPlayer2) points.player1++;
            playerPowerupsTrue(points.player1, points.toWin, playerPowerup.paddlePlayer1, playerPowerup.speedPlayer1, playerPowerup.shieldPlayer1);
            playerPowerupFalse(playerPowerup.paddlePlayer2, playerPowerup.speedPlayer2, playerPowerup.shieldPlayer2, paddle1.height, paddle2.height);
        }
        else if (ball.speed.x < 0)
        {
            if (!playerPowerup.shieldPlayer1) points.player2++;
            playerPowerupsTrue(points.player2, points.toWin, playerPowerup.paddlePlayer2, playerPowerup.speedPlayer2, playerPowerup.shieldPlayer2);
            playerPowerupFalse(playerPowerup.paddlePlayer1, playerPowerup.speedPlayer1, playerPowerup.shieldPlayer1, paddle2.height, paddle1.height);
        }

        ball.loss = true;
    }
}

void ballLoss(BALL& ball, Rectangle paddle1, Rectangle paddle2, int numberOfPlayers, POINTS points)
{
    // Posicion de la pelota cuando uno pierde

    const float paddle1PosX = paddle1.x + 30;
    const float paddle1PosY = paddle1.y + (paddle1.height / 2);
    const float paddle2PosX = paddle2.x - 15;
    const float paddle2PosY = paddle2.y + (paddle2.height / 2);

    if (points.player1 == 0 && points.player2 == 0)
    {
        ball.position.x = screenWidth / 2;
        ball.position.y = screenHeight / 2;
        if (IsKeyPressed(KEY_ENTER))
        {
            ball.speed.x *= -1.0f;
            ball.loss = false;
        }
    }
    else
    {
        if (ball.speed.x > 0)
        {
            ball.position.x = paddle1PosX;
            ball.position.y = paddle1PosY;
            if (IsKeyPressed(KEY_ENTER))
            {
                ball.speed.x *= -1.0f;
                ball.loss = false;
            }
        }
        else if (ball.speed.x < 0)
        {
            ball.position.x = paddle2PosX;
            ball.position.y = paddle2PosY;

            if (numberOfPlayers == 1)
            {
                ball.speed.x *= -1.0f;
                ball.loss = false;
            }
            else
            {
                if (IsKeyPressed(KEY_ENTER))
                {
                    ball.speed.x *= -1.0f;
                    ball.loss = false;
                }
            }
        }
    }
}

void collisionCircleRec(BALL& ball, Rectangle paddle1, Rectangle paddle2, PLAYERPOWERUPS& playerPowerup, bool reverseSpeed)
{
    // Rectangulo 1
    if (CheckCollisionCircleRec(ball.position, ball.radius, { paddle1.x, paddle1.y + (paddle1.height / 3), paddle1.width, paddle1.height / 3 }))
    {
        ball.speed.x = 5.0f;
        if (playerPowerup.speedPlayer1 || playerPowerup.speedPlayer2) ball.speed.y = 8.0f;
        else ball.speed.y = 7.0f;
        if (reverseSpeed) ball.speed.y *= -1.0f;
    }
    else if (CheckCollisionCircleRec(ball.position, ball.radius, { paddle1.x, paddle1.y, paddle1.width, paddle1.height / 3 }) ||
        CheckCollisionCircleRec(ball.position, ball.radius, { paddle1.x, paddle1.y + ((paddle1.height / 3) + (paddle1.height / 3)), paddle1.width, paddle1.height / 3 }))
    {
        ball.speed.x = 6.0f;
        if (playerPowerup.speedPlayer1 || playerPowerup.speedPlayer2) ball.speed.y = 8.0f;
        else ball.speed.y = 7.0f;
        if (reverseSpeed) ball.speed.y *= -1.0f;
    }

    // Rectangulo 2
    if (CheckCollisionCircleRec(ball.position, ball.radius, { paddle2.x, paddle2.y + (paddle2.height / 3), paddle2.width, paddle2.height / 3 }))
    {
        ball.speed.x = -5.0f;
        if (playerPowerup.speedPlayer1 || playerPowerup.speedPlayer2) ball.speed.y = 8.0f;
        else ball.speed.y = 7.0f;
        if (reverseSpeed) ball.speed.y *= -1.0f;
    }
    else if (CheckCollisionCircleRec(ball.position, ball.radius, { paddle2.x, paddle2.y, paddle2.width, paddle2.height / 3 }) ||
        CheckCollisionCircleRec(ball.position, ball.radius, { paddle2.x, paddle2.y + ((paddle2.height / 3) + (paddle2.height / 3)), paddle2.width, paddle2.height / 3 }))
    {
        ball.speed.x = -6.0f;
        if (playerPowerup.speedPlayer1 || playerPowerup.speedPlayer2) ball.speed.y = 8.0f;
        else ball.speed.y = 7.0f;
        if (reverseSpeed) ball.speed.y *= -1.0f;
    }
}