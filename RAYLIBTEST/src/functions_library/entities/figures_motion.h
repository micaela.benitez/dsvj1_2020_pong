#ifndef FIGURES_MOTION_H
#define FIGURES_MOTION_H

#include "raylib.h"

#include "ball/ball.h"
#include "paddles/paddles.h"
#include "functions_library/points/points.h"
#include "functions_library/powerups/player_powerups.h"

namespace pong
{
    namespace speed_figures_motion
    {
        const float paddlesSpeed = 5.0f;
        const float paddlesSpeedIA = 6.5f;
    }
}

int rectangleMotionUp(Rectangle& paddle);
int rectangleMotionDown(Rectangle& paddle, int screenHeight);
int rectangleMotionPC(Rectangle& paddle, BALL ball, int screenHeight);
void ballMotion(BALL& ball, POINTS& points, Rectangle& paddle1, Rectangle& paddle2, PLAYERPOWERUPS& playerPowerup);
void ballLoss(BALL& ball, Rectangle paddle1, Rectangle paddle2, int numberOfPlayers, POINTS points);
void collisionCircleRec(BALL& ball, Rectangle paddle1, Rectangle paddle2, PLAYERPOWERUPS& playerPowerup, bool reverseSpeed);

#endif