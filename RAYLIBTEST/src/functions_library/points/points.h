#ifndef POINTS_H
#define POINTS_H

struct POINTS
{
    int player1 = 0;
    int player2 = 0;
    int toWin = 12;
    int gamesWonPlayer1 = 0;
    int gamesWonPlayer2 = 0;
};

#endif