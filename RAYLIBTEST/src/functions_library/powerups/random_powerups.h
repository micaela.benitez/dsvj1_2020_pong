#ifndef RANDOM_POWERUPS_H
#define RANDOM_POWERUPS_H

#include <ctime>
#include <cstdlib>

#include "raylib.h"

#include "functions_library/entities/ball/ball.h"
#include "functions_library/keys/keys.h"
#include "functions_library/colors/colors.h"
#include "functions_library/points/points.h"
#include "player_powerups.h"
#include "functions_library/entities/figures_motion.h"

namespace pong
{
	namespace obstacle
	{
		extern float width;
		extern float height;
		extern float posX;
		extern float posY;
		extern bool up;
		extern bool down;
	}

	namespace speed_random_powerups
	{
		const float obstacleSpeed = 5.0f;
		const float obstacleMaxPosition = 0;
		const float obstacleMinPosition = screenHeight - pong::obstacle::height;
	}

	namespace positions_random_powerups
	{
		const int obstacleTextPosX = screenWidth - 650;
		const int obstacleTextPosY = screenHeight - 60;
		const int multiballTextPosX = screenWidth - 650;
		const int multiballTextPosY = 80;
		const int controlsTextPosX = screenWidth - 720;
		const int controlsTextPosY = screenHeight - 60;
		const int speedTextPosX = screenWidth - 700;
		const int speedTextPosY = screenHeight - 60;
		const int textSize = 30;
	}
}

enum RANDOMPOWERUPS_TYPES
{
	MULTIBALL = 1,
	REVERSESPEED,
	REVERSECONTROLS,
	OBSTACLES,
};

struct RANDOMPOWERUPS
{
	Vector2 position = { 0, 0 };
	int radius = 20;
	int kindOfPower = 0;
	int timer = 0;
	Color color = BLACK;
};

struct POWERUPACTIVE
{
	bool multiball = false;
	bool reverseSpeed = false;
	bool reverseControls = false;
	bool obstacles = false;
};

void randomPowerups(RANDOMPOWERUPS& powerup, BALL& ball, BALL& ball2, KEYS& key, COLOR color, POINTS& points, Rectangle paddle1, Rectangle paddle2, PLAYERPOWERUPS& playersPowerup, POWERUPACTIVE& powerupActive);
void drawRandomPowerups(RANDOMPOWERUPS& powerup, BALL& ball, BALL& ball2, POWERUPACTIVE& powerupActive);

#endif