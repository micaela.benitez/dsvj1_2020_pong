#include "player_powerups.h"

using namespace pong;
using namespace positions_player_powerups;

void powerupPaddle(float& heightRec1, float& heightRec2, int textPosX)
{
    DrawText("Powerup paddle activated!", textPosX, textPaddlePosY, textSize, BLACK);
    heightRec1 = paddleBigSize;
    heightRec2 = paddleSmallSize;
}

void powerupSpeed(int textPosX)
{
    DrawText("Powerup speed activated!", textPosX, textSpeedPosY, textSize, BLACK);
}

void powerupShield(int textPosX, int shieldPosX, Color colorShield)
{
    DrawText("Powerup shield activated!", textPosX, textShieldPosY, textSize, BLACK);
    DrawRectangle(shieldPosX, shieldPosY, shieldWidth, screenHeight, colorShield);
}

void playerPowerupsTrue(int& points, int pointsToWin, bool& powerupPaddle, bool& powerupSpeed, bool& powerupShield)
{
    // Cuando uno de los jugadores llega a cierta cantidad de puntos, se le activa un potenciador

    const int halfPoints = pointsToWin / 2;
    const int halfOfHalfPoints = (pointsToWin / 2) / 2;

    if (points == (halfPoints - halfOfHalfPoints)) powerupPaddle = true;
    else if (points == halfPoints) powerupSpeed = true;
    else if (points == (halfPoints + halfOfHalfPoints)) powerupShield = true;
}

void playerPowerupFalse(bool& powerupPaddle, bool& powerupSpeed, bool& powerupShield, float& rec1Height, float& rec2Height)
{
    // Si tiene activo un potenciador y pierde la pelota, lo pierde
    if (powerupPaddle)
    {
        powerupPaddle = false;
        rec1Height = paddleNormalSize;
        rec2Height = paddleNormalSize;
    }
    if (powerupSpeed) powerupSpeed = false;
    if (powerupShield) powerupShield = false;
}