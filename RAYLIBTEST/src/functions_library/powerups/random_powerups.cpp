#include "random_powerups.h"

namespace pong
{
    namespace obstacle
    {
        float width = 20;
        float height = 150;
        float posX = (screenWidth / 2) - 10;
        float posY = 0;
        bool up = false;
        bool down = true;
    }
}

using namespace pong;
using namespace speed_random_powerups;
using namespace positions_random_powerups;

void powerupMultiball(BALL& ball2, COLOR color, POINTS& points, Rectangle paddle1, Rectangle paddle2, PLAYERPOWERUPS& playerPowerup, POWERUPACTIVE& powerupActive)
{    
    DrawCircleV(ball2.position, ball2.radius, color.ball);

    ball2.position.x += ball2.speed.x;
    ball2.position.y += ball2.speed.y;
    if ((ball2.position.y >= (GetScreenHeight() - ball2.radius)) || (ball2.position.y <= ball2.radius)) ball2.speed.y *= -1.0f;
    if ((ball2.position.x >= (GetScreenWidth() - ball2.radius)) || (ball2.position.x <= ball2.radius))
    {
        powerupActive.multiball = false;
        ball2.position = { screenWidth / 2, screenHeight / 2 };

        if (ball2.speed.x > 0)
        {
            if (!playerPowerup.shieldPlayer2) points.player1++;
            playerPowerupsTrue(points.player1, points.toWin, playerPowerup.paddlePlayer1, playerPowerup.speedPlayer1, playerPowerup.shieldPlayer1);
            playerPowerupFalse(playerPowerup.paddlePlayer2, playerPowerup.speedPlayer2, playerPowerup.shieldPlayer2, paddle1.height, paddle2.height);
        }
        else if (ball2.speed.x < 0)
        {
            if (!playerPowerup.shieldPlayer1) points.player2++;
            playerPowerupsTrue(points.player2, points.toWin, playerPowerup.paddlePlayer2, playerPowerup.speedPlayer2, playerPowerup.shieldPlayer2);
            playerPowerupFalse(playerPowerup.paddlePlayer1, playerPowerup.speedPlayer1, playerPowerup.shieldPlayer1, paddle2.height, paddle1.height);
        }
    }

    collisionCircleRec(ball2, paddle1, paddle2, playerPowerup, powerupActive.reverseSpeed);

    if (powerupActive.obstacles)
    {
        if (CheckCollisionCircleRec(ball2.position, ball2.radius, { pong::obstacle::posX, pong::obstacle::posY, pong::obstacle::width, pong::obstacle::height })) ball2.speed.x *= -1.0f;
    }
}

void powerupReverseControls(KEYS& key, POWERUPACTIVE powerupActive)
{
    if (powerupActive.reverseControls)
    {
        keys(key.upPlayer1, key.numberKeyDownPlayer1);
        keys(key.upPlayer2, key.numberKeyDownPlayer2);
        keys(key.downPlayer1, key.numberKeyUpPlayer1);
        keys(key.downPlayer2, key.numberKeyUpPlayer2);
    }
    else
    {
        keys(key.upPlayer1, key.numberKeyUpPlayer1);
        keys(key.upPlayer2, key.numberKeyUpPlayer2);
        keys(key.downPlayer1, key.numberKeyDownPlayer1);
        keys(key.downPlayer2, key.numberKeyDownPlayer2);
    }
}

void powerupObstacles(BALL& ball, BALL& ball2)
{    
    DrawRectangleGradientV(pong::obstacle::posX, pong::obstacle::posY, pong::obstacle::width, pong::obstacle::height, MAGENTA, BLACK);

    if (!ball.loss)
    {
        if (pong::obstacle::up)
        {
            pong::obstacle::posY -= obstacleSpeed;

            if (pong::obstacle::posY == obstacleMaxPosition)
            {
                pong::obstacle::up = false;
                pong::obstacle::down = true;
            }
        }
        else if (pong::obstacle::down)
        {
            pong::obstacle::posY += obstacleSpeed;

            if (pong::obstacle::posY == obstacleMinPosition)
            {
                pong::obstacle::down = false;
                pong::obstacle::up = true;
            }
        }

        if (CheckCollisionCircleRec(ball.position, ball.radius, { pong::obstacle::posX, pong::obstacle::posY, pong::obstacle::width, pong::obstacle::height }))
        {
            ball.speed.x *= -1.0f;
        }
    }
}

void randomPowerups(RANDOMPOWERUPS& powerup, BALL& ball, BALL& ball2, KEYS& key, COLOR color, POINTS& points, Rectangle paddle1, Rectangle paddle2, PLAYERPOWERUPS& playerPowerup, POWERUPACTIVE& powerupActive)
{
    srand(time(NULL));
    int numbersX = screenWidth - (powerup.radius * 2);
    int numbersY = screenHeight - (powerup.radius * 2);
    int cantPowerups = 4;
    powerup.timer += 1;

    if (powerup.timer % 1500 == 0 || powerup.timer == 1)   // Cada 15 segundos aparece un potenciador distinto (cuando el tiempo esta en 1 tambien asi aparece un una posicion random)
    {
        powerup.position.x = rand() % numbersX + powerup.radius;
        powerup.position.y = rand() % numbersY + powerup.radius;
        powerup.kindOfPower = rand() % cantPowerups + 1;

        if (powerup.kindOfPower == MULTIBALL) powerup.color = RED;
        else if (powerup.kindOfPower == REVERSESPEED) powerup.color = BLUE;
        else if (powerup.kindOfPower == REVERSECONTROLS) powerup.color = GREEN;

        if (powerupActive.reverseSpeed) powerupActive.reverseSpeed = false;
        if (powerupActive.reverseControls)
        {
            powerupActive.reverseControls = false;
            powerupReverseControls(key, powerupActive);
        }        
    }

    if (CheckCollisionCircles(powerup.position, powerup.radius, ball.position, ball.radius) ||
        CheckCollisionCircles(powerup.position, powerup.radius, ball2.position, ball2.radius))
    {
        if (powerup.kindOfPower == MULTIBALL)
        {
            powerupActive.multiball = true;   
            ball2.speed.y = -ball.speed.y;
            ball2.speed.x = -ball.speed.x;
        }
        else if (powerup.kindOfPower == REVERSESPEED) powerupActive.reverseSpeed = true;
        else if (powerup.kindOfPower == REVERSECONTROLS)
        {
            powerupActive.reverseControls = true;
            powerupReverseControls(key, powerupActive);
        }        
    }

    if (!powerupActive.multiball) ball2.position = ball.position;
    else powerupMultiball(ball2, color, points, paddle1, paddle2, playerPowerup, powerupActive);
}

void drawRandomPowerups(RANDOMPOWERUPS& powerup, BALL& ball, BALL& ball2, POWERUPACTIVE& powerupActive)
{
    if ((!powerupActive.multiball && !powerupActive.reverseControls && !powerupActive.reverseSpeed) && powerup.kindOfPower != OBSTACLES)
    {
        DrawCircleGradient(powerup.position.x, powerup.position.y, powerup.radius, powerup.color, BLACK);
    }
    else if (powerup.kindOfPower == OBSTACLES)
    {
        powerupObstacles(ball, ball2);
        DrawText("Activated obstacle!", obstacleTextPosX, obstacleTextPosY, textSize, BLACK);
    }

    if (powerupActive.multiball) DrawText("Activated multiball!", multiballTextPosX, multiballTextPosY, textSize, BLACK);
    else if (powerupActive.reverseControls) DrawText("Reverse controls activated!", controlsTextPosX, controlsTextPosY, textSize, BLACK);
    else if (powerupActive.reverseSpeed) DrawText("Reverse speed activated!", speedTextPosX, speedTextPosY, textSize, BLACK);
}