#ifndef PLAYER_POWERUPS_H
#define PLAYER_POWERUPS_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"

namespace pong
{
    namespace positions_player_powerups
    {
        const int textPaddlePosY = screenHeight - 40;
        const int textSpeedPosY = screenHeight - 55;
        const int textShieldPosY = screenHeight - 70;
        const int textSize = 20;

        const int shieldPosY = 0;
        const int shieldWidth = 5;

        const int paddleNormalSize = 120;
        const int paddleBigSize = 150;
        const int paddleSmallSize = 90;
    }
}

struct PLAYERPOWERUPS
{
	bool paddlePlayer1 = false;
	bool speedPlayer1 = false;
	bool shieldPlayer1 = false;

	bool paddlePlayer2 = false;
	bool speedPlayer2 = false;
	bool shieldPlayer2 = false;
};

void powerupPaddle(float& heightRec1, float& heightRec2, int textPosX);
void powerupSpeed(int textPosX);
void powerupShield(int textPosX, int shieldPosX, Color colorShield);
void playerPowerupsTrue(int& points, int pointsToWin, bool& powerupPaddle, bool& powerupSpeed, bool& powerupShield);
void playerPowerupFalse(bool& powerupPaddle, bool& powerupSpeed, bool& powerupShield, float& rec1Height, float& rec2Height);

#endif