#include "keys.h"

void keys(int& key, int& numberKey, int key1, int key2, int key3)
{
    // Opcion keys
    if (IsKeyPressed(KEY_LEFT)) numberKey--;
    else if (IsKeyPressed(KEY_RIGHT)) numberKey++;

    if (numberKey == 1) key = KEY_A;
    else if (numberKey == 2) key = KEY_B;
    else if (numberKey == 3) key = KEY_C;
    else if (numberKey == 4) key = KEY_D;
    else if (numberKey == 5) key = KEY_E;
    else if (numberKey == 6) key = KEY_F;
    else if (numberKey == 7) key = KEY_G;
    else if (numberKey == 8) key = KEY_H;
    else if (numberKey == 9) key = KEY_I;
    else if (numberKey == 10) key = KEY_J;
    else if (numberKey == 11) key = KEY_K;
    else if (numberKey == 12) key = KEY_L;
    else if (numberKey == 13) key = KEY_M;
    else if (numberKey == 14) key = KEY_N;
    else if (numberKey == 15) key = KEY_O;
    else if (numberKey == 16) key = KEY_P;
    else if (numberKey == 17) key = KEY_Q;
    else if (numberKey == 18) key = KEY_R;
    else if (numberKey == 19) key = KEY_S;
    else if (numberKey == 20) key = KEY_T;
    else if (numberKey == 21) key = KEY_U;
    else if (numberKey == 22) key = KEY_V;
    else if (numberKey == 23) key = KEY_W;
    else if (numberKey == 24) key = KEY_X;
    else if (numberKey == 25) key = KEY_Y;
    else if (numberKey == 26) key = KEY_Z;
    else if (numberKey == 27) key = KEY_UP;
    else if (numberKey == 28) key = KEY_DOWN;

    if (numberKey <= 0) numberKey = 28;
    else if (numberKey >= 29) numberKey = 1;

    if ((numberKey == key1 || numberKey == key2 || numberKey == key3) && IsKeyPressed(KEY_RIGHT)) numberKey++;
    if ((numberKey == key1 || numberKey == key2 || numberKey == key3) && IsKeyPressed(KEY_LEFT)) numberKey--;
}

void keys(int& key, int& numberKey)
{
    // Opcion keys
    if (IsKeyPressed(KEY_LEFT)) numberKey--;
    else if (IsKeyPressed(KEY_RIGHT)) numberKey++;

    if (numberKey == 1) key = KEY_A;
    else if (numberKey == 2) key = KEY_B;
    else if (numberKey == 3) key = KEY_C;
    else if (numberKey == 4) key = KEY_D;
    else if (numberKey == 5) key = KEY_E;
    else if (numberKey == 6) key = KEY_F;
    else if (numberKey == 7) key = KEY_G;
    else if (numberKey == 8) key = KEY_H;
    else if (numberKey == 9) key = KEY_I;
    else if (numberKey == 10) key = KEY_J;
    else if (numberKey == 11) key = KEY_K;
    else if (numberKey == 12) key = KEY_L;
    else if (numberKey == 13) key = KEY_M;
    else if (numberKey == 14) key = KEY_N;
    else if (numberKey == 15) key = KEY_O;
    else if (numberKey == 16) key = KEY_P;
    else if (numberKey == 17) key = KEY_Q;
    else if (numberKey == 18) key = KEY_R;
    else if (numberKey == 19) key = KEY_S;
    else if (numberKey == 20) key = KEY_T;
    else if (numberKey == 21) key = KEY_U;
    else if (numberKey == 22) key = KEY_V;
    else if (numberKey == 23) key = KEY_W;
    else if (numberKey == 24) key = KEY_X;
    else if (numberKey == 25) key = KEY_Y;
    else if (numberKey == 26) key = KEY_Z;
    else if (numberKey == 27) key = KEY_UP;
    else if (numberKey == 28) key = KEY_DOWN;

    if (numberKey == 0) numberKey = 28;
    else if (numberKey == 29) numberKey = 1;
}

void showKeys(int key, int posX, int posY)
{
    const int keyPosX = posX - 13;
    const int keyPosY = (screenHeight / 2) + posY;
    const int keySize = 30;
    const int keySize2 = 20;

    if (key == KEY_A) DrawText("A", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_B) DrawText("B", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_C) DrawText("C", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_D) DrawText("D", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_E) DrawText("E", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_F) DrawText("F", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_G) DrawText("G", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_H) DrawText("H", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_I) DrawText("I", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_J) DrawText("J", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_K) DrawText("K", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_L) DrawText("L", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_M) DrawText("M", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_N) DrawText("N", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_O) DrawText("O", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_P) DrawText("P", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_Q) DrawText("Q", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_R) DrawText("R", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_S) DrawText("S", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_T) DrawText("T", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_U) DrawText("U", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_V) DrawText("V", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_W) DrawText("W", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_X) DrawText("X", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_Y) DrawText("Y", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_Z) DrawText("Z", posX, keyPosY, keySize, BLACK);
    else if (key == KEY_UP) DrawText("up", posX, keyPosY, keySize2, BLACK);
    else if (key == KEY_DOWN) DrawText("down", keyPosX, keyPosY, keySize2, BLACK);
}