#ifndef KEYS_H
#define KEYS_H

#include "raylib.h"

#include "functions_library/screen_size/screen_size.h"

struct KEYS
{
	int upPlayer1 = KEY_W;
	int downPlayer1 = KEY_S;
	int upPlayer2 = KEY_UP;
	int downPlayer2 = KEY_DOWN;

	int numberKeyUpPlayer1 = 23;
	int numberKeyDownPlayer1 = 19;
	int numberKeyUpPlayer2 = 27;
	int numberKeyDownPlayer2 = 28;
};

void keys(int& key, int& numberKey, int key1, int key2, int key3);
void keys(int& key, int& numberKey);
void showKeys(int key, int posX, int posY);

#endif